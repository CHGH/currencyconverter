object Main {

  def main(args: Array[String]): Unit = {

    println("Hello, World!")

    val list: List[Any] = List("a string", 732, 'c', true, () => "f")
    list foreach println
  }
}
